terraform {
  required_version = "~> 0.12"
  backend "remote" {
    organization = "fstn"

    workspaces {
      name = "google-cloud-ops"
    }
  }
}
